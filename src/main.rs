// This file is part of the seehecht source code.
//
// ©️ 2022 papojari <mailto:papojari-git.ovoid@aleeas.com> <https://matrix.to/#/@papojari:artemislena.eu> <https://papojari.codeberg.page>
//
// For the license information, please view the README.md file that was distributed with this source code.

use chrono::Local;
use clap::Parser;
use std::env;
use std::fs::File;
use std::io::Write;
use std::process::Command;

#[derive(Parser, Debug)]
#[clap(
    name = "seehecht",
    version,
    about = "A tool to quickly open a markdown document with already filled out frontmatter."
)]
struct Args {
    /// Document title
    #[clap(long, short)]
    title: String,

    /// Output to stdout. The output won't be able to open in your editor.
    #[clap(long, short)]
    stdout: bool,

    /// Don't open file in editor.
    #[clap(long, short)]
    no_editor: bool,

    /// Frontmatter type.
    #[clap(long, short, possible_values = ["yaml", "toml", "json"], default_value = "yaml")]
    frontmatter: String,

    /// Quote the date in the frontmatter. If the frontmatter type is JSON this won't do anything because the date is already quoted in JSON.
    #[clap(long, short)]
    quoted_date: bool,

    /// Frontmatter date format.
    #[clap(long, short, possible_values = ["iso8601", "rfc3339"], default_value = "iso8601")]
    date_format: String,
}

fn main() {
    let args = Args::parse();
    let title = args.title;
    // Choose either ISO8601 or RFC3339 depending what the date format option is. The code looks like the formats are reversed but this should be right according to `date --iso-8601` and `date --rfc-3339`.
    let datetime = match args.date_format.as_str() {
        "iso8601" => Local::now().to_rfc3339(),
        "rfc3339" => Local::now().to_string(),
        _ => panic!("Clap parsed invalid date format."),
    };
    let quoted_date = args.quoted_date;
    let frontmatter_type = args.frontmatter.as_str();
    let filename = format!("{}.md", title);
    let stdout = args.stdout;
    let open_in_editor = match args.no_editor {
        true => false,
        false => true,
    };
    let editor = env::var("EDITOR").expect("The editor EDITOR environment variable is not set.\n\nYou can set it to the executable of your favorite text editor like this: `export EDITOR=executable`");

    // Creating the file content.
    let file_content = match frontmatter_type {
        "yaml" => match quoted_date {
            true => format!("---\ntitle: \"{}\"\ndate: \"{}\"\n---\n\n", title, datetime),
            false => format!("---\ntitle: \"{}\"\ndate: {}\n---\n\n", title, datetime),
        },
        "toml" => match quoted_date {
            true => format!(
                "+++\ntitle = \"{}\"\ndate = \"{}\"\n+++\n\n",
                title, datetime
            ),
            false => format!("+++\ntitle = \"{}\"\ndate = {}\n+++\n\n", title, datetime),
        },
        "json" => format!(
            "{{\n  \"title\" = \"{}\",\n  \"date\" = \"{}\"\n}}\n\n",
            title, datetime
        ),
        _ => panic!("Clap parsed invalid frontmatter type."),
    };

    if stdout {
        // Print file content.
        print!("{}", file_content);
    } else {
        // Creating the file.
        let mut file = File::create(&filename).expect("create failed");
        file.write_all(file_content.as_bytes())
            .expect("write failed");

        if open_in_editor {
            Command::new(&editor)
                .arg(&filename)
                .status()
                .expect("Failed to open {&filename} in {&editor}");
        };
    }
}
