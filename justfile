#!/usr/bin/env just --justfile
NAME:='seehecht'
FANCY_NAME:=NAME+" 🐟"

# By default, recipes are only listed.
default:
	@just --list

# Compile and run.
run:
	@cargo run

# Compile.
build:
	@cargo build

# Uninstall for user only.
user-uninstall:
	@cargo uninstall seehecht
	@rm ~/.cargo/bin/seehecht

# Uninstall for system only.
system-uninstall:
	@cargo uninstall seehecht --root /usr
	@rm /usr/bin/seehecht

# Uninstall for custom path only.
custom-uninstall:
	@cargo uninstall seehecht --root "$SEEHECHT_INSTALL_PATH"
	@rm "$SEEHECHT_INSTALL_PATH"/bin/seehecht

# Install for user only.
user-install:
	@cargo install --path .
	@ln -s ~/.cargo/bin/seh ~/.cargo/bin/seehecht

# Install systemwide.
system-install:
	@cargo install --path . --root /usr --no-track
	@ln -s /usr/bin/seh /bin/seehecht

# Install to custom path.
custom-install:
	@cargo install --path . --root "$SEEHECHT_INSTALL_PATH" --no-track
	@ln -s "$SEEHECHT_INSTALL_PATH"/bin/seh "$SEEHECHT_INSTALL_PATH"/bin/seehecht

